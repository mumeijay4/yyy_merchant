import 'dart:async';
import 'dart:io';

import 'package:YYYMErchant/Beranda.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
// import 'package:dio/dio.dart';
import 'package:permission_handler/permission_handler.dart';

// import 'beranda.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await FlutterDownloader.initialize(
  //     debug: true // optional: set false to disable printing logs to console
  // );
  await Permission.storage.request();
  await Permission.camera.request();
  await Permission.notification.request();
  runApp(new MyApp());
}
String tokenFirebase;

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'YukYakYuk Merchant',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          brightness: Brightness.light,
          backgroundColor: const Color(0xFFfbfbfd),
          primaryColorLight: Colors.white,
          primaryColorBrightness: Brightness.light,
          primaryColor: Colors.white),
      home: SplashScreen(),
      routes: <String, WidgetBuilder>{
        "home": (BuildContext context) => Beranda(tokenFirebase)
      }
    );
  }
}


class SplashScreen extends StatefulWidget {
@override
  _SplashScreenState createState() => _SplashScreenState();
}

Future<dynamic> myBackgroundHandler(Map<String, dynamic> message){
  return _SplashScreenState()._showNotification(message);
}

class _SplashScreenState extends State<SplashScreen> {
  final FirebaseMessaging _fcm = FirebaseMessaging();
  // Dio dio = Dio();

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  Future _showNotification(Map<String, dynamic> message) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
      'channel id',
      'channel name',
      'channel desc',
      importance: Importance.Max,
      priority: Priority.High,
    );
 
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, null);
    await flutterLocalNotificationsPlugin.show(
        0,
      'Delivery',
      '${message["notification"]["body"]}',
      platformChannelSpecifics,
      payload: 'Default_Sound',
    );
  }

  Future selectNotification(String payload)async{
    await flutterLocalNotificationsPlugin.cancelAll();
  }

  @override
  void initState() {
        var initializationSettingsAndroid = AndroidInitializationSettings('@mipmap/ic_launcher');
        var initializationSettings = InitializationSettings(
            initializationSettingsAndroid, null);
        flutterLocalNotificationsPlugin.initialize(initializationSettings,
            onSelectNotification: selectNotification);
    super.initState();
    check();
    _fcm.configure(
      onBackgroundMessage:myBackgroundHandler,
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        _showNotification(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );
    _fcm.getToken().then((token) => setState(() {
      print(token);
          tokenFirebase = token;
      print(tokenFirebase);
    }));
    // _fcm.getToken();

    // initConnectivity();
    // _connectivitySubscription =
    //     _connectivity.onConnectivityChanged.listen();
  }


  String connected;
  check() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        if (connected == "connected") {
          print("object");
        } else {
        connected = "connected";
        Timer(Duration(seconds: 3),
            () => Navigator.of(context).pushReplacementNamed(_navto));
        }
      }
    } on SocketException catch (_) {
      return showDialog(
          context: context,
          builder: (_) => AlertDialog(
            shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.grey[100], width: 1),
              borderRadius: BorderRadius.circular(10),
            ),
            content: Container(
              height: 90,
              child: Center(
                child: Column(
                  children: <Widget>[
                    Icon(
                      Icons.signal_cellular_connected_no_internet_4_bar,
                      size: 50.0,
                      color: Colors.green[400],
                    ),
                    SizedBox(height: 10,),
                    Text("Jaringan tidak tersambung", style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold, fontSize: 14,))
                  ],
                ),
              )
            ),
          )
        );
    }
  }

  


  String _navto = "home";
  didChangeDependencies() {
    super.didChangeDependencies();
    Timer(Duration(seconds: 30), () {
      check();
    });
  }


  Widget build(BuildContext context) {
    final Size _screenSize = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Colors.white,
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
              Image(
                  width: _screenSize.width * .3,
                  fit: BoxFit.fitWidth,
                  image: AssetImage("assets/logo.png")
                  ),
              SizedBox(height: 40),
              Text('for Merchant',
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: 14,
                      fontWeight: FontWeight.w700))
            ])));
  }
}
