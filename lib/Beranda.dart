import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:location/location.dart';

const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Mobile Safari/537.36';

class Beranda extends StatefulWidget {
  final String token;
  const Beranda(this.token);
  // final String url;
  // Beranda(this.url);
  @override
  BerandaState createState() => BerandaState();
}

class BerandaState extends State<Beranda> {
  Location location = Location();

  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  LocationData _locationData;

  String _navto = "home";

  final flutterWebviewPlugin = FlutterWebviewPlugin();

  didChangeDependencies() async {
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _locationData = await location.getLocation();
    check();

    flutterWebviewPlugin.onUrlChanged.listen((String url) {
      print('new url: $url');
      print(
          'LOGOUT: ${url.contains('http://pub.keira145.net/auth/logout?type=merchant')}');
      print('LOGOUT2: ${url == 'http://pub.keira145.net/auth/merchant'}');
      // if (url.contains('http://pub.keira145.net/auth/logout?type=merchant')) {
      if (url == 'http://pub.keira145.net/auth/merchant') {
        flutterWebviewPlugin.close();
        SystemNavigator.pop();
      }
    });

    super.didChangeDependencies();
  }

  String connected = 'connected';
  check() async {
    print(widget.token +
        "oakwdoakdoakkwdoakwodkaodkaowdkaowokdaowdkaowkdaowkdowak");
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        if (connected == "connected") {
          print("connected");
        } else {
          connected = "connected";
          Timer(Duration(seconds: 3),
              () => Navigator.of(context).pushReplacementNamed(_navto));
        }
      }
    } on SocketException catch (_) {
      connected = '';
      return showDialog(
          context: context,
          builder: (_) => AlertDialog(
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.grey[100], width: 1),
                  borderRadius: BorderRadius.circular(10),
                ),
                content: Container(
                    height: 90,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.signal_cellular_connected_no_internet_4_bar,
                            size: 50.0,
                            color: Colors.green[400],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text("Jaringan tidak tersambung",
                              style: TextStyle(
                                color: Colors.black54,
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                              ))
                        ],
                      ),
                    )),
              ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
        // url: "https://www.youtube.com/",
        // url: "https://dori.integrasolusimandiri.com/delivery/",
        // url: "https://dori.integrasolusimandiri.com/",
        url:
            "http://pub.keira145.net/store-owner/dashboard?firebase_token=${widget.token}",
        // url: "https://yukyakyuk.id",
        // url: "https://yukyakyuk.id/delivery",
        resizeToAvoidBottomInset: true,
        withZoom: true,
        withJavascript: true,
        withLocalStorage: true,
        supportMultipleWindows: true,
        userAgent: kAndroidUserAgent,
        ignoreSSLErrors: true,
        geolocationEnabled: true,
        initialChild:
            Container(child: const Center(child: Text('Waiting.....'))),
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(0), // here the desired height
            child: AppBar()));
  }
}
